// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;

namespace CodeProject.ObjectPool;

/// <summary>
///     Exception which can be thrown from <see cref="PooledObject.ResetState" /> delegate
///     in order to let the pool know that the object could not be successfully reset and that it should be released.
/// </summary>
public class ResetStateFailureException : Exception
{
    /// <summary>
    ///     Constructor.
    /// </summary>
    public ResetStateFailureException()
    {
    }

    /// <summary>
    ///     Constructor.
    /// </summary>
    /// <param name="message">Message.</param>
    public ResetStateFailureException(string message)
        : base(message)
    {
    }

    /// <summary>
    ///     Constructor.
    /// </summary>
    /// <param name="message">Message.</param>
    /// <param name="innerException">Inner exception.</param>
    public ResetStateFailureException(string message, Exception innerException)
        : base(message, innerException)
    {
    }
}
