// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CodeProject.ObjectPool;

/// <summary>
///   Default implementation of <see cref="IEvictionTimer"/>.
/// </summary>
public sealed class EvictionTimer : IEvictionTimer
{
    private readonly Dictionary<Guid, Timer> _actionMap = new();
    private volatile bool _disposed;

    /// <summary>
    ///   Finalizer for <see cref="EvictionTimer"/>.
    /// </summary>
    ~EvictionTimer()
    {
        Dispose(false);
    }

    /// <summary>
    ///   Cancels a scheduled eviction action using a ticket returned by <see cref="Schedule(Action, TimeSpan, TimeSpan)"/>.
    /// </summary>
    /// <param name="actionTicket">
    ///   An eviction action ticket, which has been returned by <see cref="Schedule(Action, TimeSpan, TimeSpan)"/>.
    /// </param>
    public void Cancel(Guid actionTicket)
    {
        ThrowIfDisposed();
        lock (_actionMap)
        {
            if (_actionMap.TryGetValue(actionTicket, out var timer))
            {
                _actionMap.Remove(actionTicket);
                timer.Dispose();
            }
        }
    }

    /// <summary>
    ///   Disposes the eviction timer, making it unusable.
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    /// <summary>
    ///   Schedules an eviction action.
    /// </summary>
    /// <param name="action">Eviction action.</param>
    /// <param name="delay">Start delay.</param>
    /// <param name="period">Schedule period.</param>
    /// <returns>
    ///   A ticket which identifies the scheduled eviction action, it can be used to cancel the
    ///   scheduled action via <see cref="Cancel(Guid)"/> method.
    /// </returns>
    public Guid Schedule(Action action, TimeSpan delay, TimeSpan period)
    {
        ThrowIfDisposed();

        if (action == null)
        {
            return Guid.Empty;
        }

        lock (_actionMap)
        {
            void TimerCallback()
            {
                ObjectPool.Logger?.Invoke(null, "Begin scheduled evictor", null);
                action();
                ObjectPool.Logger?.Invoke(null, "End scheduled evictor", null);
            }

            var actionTicket = Guid.NewGuid();
            _actionMap[actionTicket] = new Timer(_ => TimerCallback(), null, delay, period);
            return actionTicket;
        }
    }

    /// <summary>
    ///   Disposes all action timers.
    /// </summary>
    /// <param name="disposing">False if called by the finalizer, true otherwise.</param>
    private void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            // Mark this object as completely disposed.
            _disposed = true;

            if (disposing && _actionMap != null)
            {
                var timers = _actionMap.Values.ToArray();
                _actionMap.Clear();
                foreach (var t in timers)
                {
                    t.Dispose();
                }
            }
        }
    }

    private void ThrowIfDisposed()
    {
        if (_disposed)
        {
            throw new ObjectDisposedException(GetType().FullName);
        }
    }
}
