/*
 * Generic Object Pool Implementation
 *
 * Implemented by Ofir Makmal, 28/1/2013
 *
 * My Blog: Blogs.microsoft.co.il/blogs/OfirMakmal
 * Email:   Ofir.Makmal@gmail.com
 *
 */

using System;
using System.Reflection;

namespace CodeProject.ObjectPool;

/// <summary>
///     Helper class for building pooled object wrappers.
/// </summary>
public static class PooledObjectWrapper
{
    /// <summary>
    ///     Wraps a given resource so that it can be put in the pool.
    /// </summary>
    /// <typeparam name="T">The type of the resource.</typeparam>
    /// <param name="resource">The resource to be wrapped.</param>
    /// <exception cref="ArgumentNullException">Given resource is null.</exception>
    /// <returns>A wrapper for given resource.</returns>
    public static PooledObjectWrapper<T> Create<T>(T resource) where T : class
    {
        return new PooledObjectWrapper<T>(resource);
    }
}

/// <summary>
///     PooledObject wrapper, for classes which cannot inherit from that class.
/// </summary>
/// <typeparam name="T">The type of the resource.</typeparam>
[Serializable]
public sealed class PooledObjectWrapper<T> : PooledObject where T : class
{
    /// <summary>
    ///     Wraps a given resource so that it can be put in the pool.
    /// </summary>
    /// <param name="resource">The resource to be wrapped.</param>
    public PooledObjectWrapper(T resource)
    {
        InternalResource = resource;

        base.OnReleaseResources += () => OnReleaseResources?.Invoke(InternalResource);
        base.OnResetState += () => OnResetState?.Invoke(InternalResource);

        if (IResettableTypeProxy.IsInterfaceImplementedBy(InternalResource))
        {
            base.OnResetState += () => IResettableTypeProxy.TryReset(InternalResource);
        }
    }

    /// <summary>
    ///     The resource wrapped inside this class.
    /// </summary>
    public T InternalResource { get; }

    /// <summary>
    ///     Triggered by the pool manager when there is no need for this object anymore.
    /// </summary>
    public new Action<T>? OnReleaseResources { get; set; }

    /// <summary>
    ///     Triggered by the pool manager just before the object is being returned to the pool.
    ///     If reset cannot be successfully performed, delegates can throw <see cref="ResetStateFailureException" />
    ///     in order to let the pool know that the object should be released.
    /// </summary>
    public new Action<T>? OnResetState { get; set; }
}

internal static class IResettableTypeProxy
{
    private static readonly Type? s_interfaceType = Type.GetType("Microsoft.Extensions.ObjectPool.IResettable, Microsoft.Extensions.ObjectPool");
    private static readonly MethodInfo? s_tryResetMethodInfo = s_interfaceType?.GetMethod("TryReset");

    public static bool IsInterfaceImplementedBy(object resource)
    {
        return s_interfaceType?.IsInstanceOfType(resource) == true;
    }

    public static void TryReset(object resource)
    {
        var result = (bool)s_tryResetMethodInfo!.Invoke(resource, Array.Empty<object>())!;
        if (!result)
        {
            throw new ResetStateFailureException();
        }
    }
}
