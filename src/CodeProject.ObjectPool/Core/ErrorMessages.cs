// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace CodeProject.ObjectPool.Core;

/// <summary>
///   Static class containing all error messages used by ObjectPool.
/// </summary>
internal static class ErrorMessages
{
    public const string AsyncFactoryForSyncGetObject =
        "Async factories cannot be used for sync GetObject operations. Please use GetObjectAsync.";
    public const string NegativeOrZeroMaximumPoolSize =
        "Maximum pool size must be greater than zero.";
    public const string NegativeOrZeroTimeout = "Timeout must be greater than zero.";
    public const string NullResource = "Resource cannot be null.";
}
