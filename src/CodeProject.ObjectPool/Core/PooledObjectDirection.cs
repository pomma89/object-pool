// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace CodeProject.ObjectPool.Core;

/// <summary>
///   Direction of a pooled object.
/// </summary>
public enum PooledObjectDirection
{
    /// <summary>
    ///   An object is returning to the pool.
    /// </summary>
    Inbound,

    /// <summary>
    ///   An object is getting out of the pool.
    /// </summary>
    Outbound
}
