// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace CodeProject.ObjectPool.Core;

/// <summary>
///   Exposes a way to return objects to the pool.
/// </summary>
internal interface IObjectPoolHandle
{
    void ReturnObjectToPool(PooledObject objectToReturnToPool, bool reRegisterForFinalization);
}
