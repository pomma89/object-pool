// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using BenchmarkDotNet.Attributes;

namespace CodeProject.ObjectPool.Benchmarks;

[Config(typeof(Program.Config))]
public class RetrieveOneObject : RetrieveObjectsBase
{
    [Benchmark]
    public void AdaptedMicrosoft()
    {
        MyResource? res = null;
        try
        {
            res = AdaptedMicrosoftObjectPool.Get();
        }
        finally
        {
            if (res != null)
            {
                AdaptedMicrosoftObjectPool.Return(res);
            }
        }
    }

    [Benchmark]
    public void Microsoft()
    {
        MyResource? res = null;
        try
        {
            res = MicrosoftObjectPool.Get();
        }
        finally
        {
            if (res != null)
            {
                MicrosoftObjectPool.Return(res);
            }
        }
    }

    [Benchmark]
    public void Parameterized()
    {
        using var x = ParamObjectPool.GetObject(21);
    }

    [Benchmark(Baseline = true)]
    public void Simple()
    {
        using var x = ObjectPool.GetObject();
    }

    [Benchmark]
    public void Resettable()
    {
        using var x = ResettableObjectPool.GetObject();
    }

    [Benchmark]
    public void Wrapped()
    {
        using var x = WrappedObjectPool.GetObject();
    }
}
