// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using BenchmarkDotNet.Analysers;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Diagnosers;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Running;

namespace CodeProject.ObjectPool.Benchmarks;

public static class Program
{
    public static void Main(string[] args)
    {
        new BenchmarkSwitcher(
            new[]
            {
                typeof(MemoryStreamPooling),
                typeof(RetrieveOneObject),
                typeof(RetrieveObjectsConcurrently)
            }
        ).Run(args);
    }

    public class Config : ManualConfig
    {
        public Config()
        {
            AddJob(Job.Default);
            AddExporter(PlainExporter.Default, RPlotExporter.Default);
            AddDiagnoser(MemoryDiagnoser.Default);
            AddAnalyser(EnvironmentAnalyser.Default);
        }
    }
}
