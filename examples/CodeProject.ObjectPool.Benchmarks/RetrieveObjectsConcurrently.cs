// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;

namespace CodeProject.ObjectPool.Benchmarks;

[Config(typeof(Program.Config))]
public class RetrieveObjectsConcurrently : RetrieveObjectsBase
{
    [Params(10, 100, 1000)]
    public int Count { get; set; }

    [Benchmark]
    public ParallelLoopResult AdaptedMicrosoft() =>
        Parallel.For(
            0,
            Count,
            _ =>
            {
                MyResource? res = null;
                try
                {
                    res = AdaptedMicrosoftObjectPool.Get();
                }
                finally
                {
                    if (res != null)
                    {
                        AdaptedMicrosoftObjectPool.Return(res);
                    }
                }
            }
        );

    [Benchmark]
    public ParallelLoopResult Microsoft() =>
        Parallel.For(
            0,
            Count,
            _ =>
            {
                MyResource? res = null;
                try
                {
                    res = MicrosoftObjectPool.Get();
                }
                finally
                {
                    if (res != null)
                    {
                        MicrosoftObjectPool.Return(res);
                    }
                }
            }
        );

    [Benchmark]
    public ParallelLoopResult Parameterized() =>
        Parallel.For(
            0,
            Count,
            _ =>
            {
                using var x = ParamObjectPool.GetObject(21);
            }
        );

    [Benchmark(Baseline = true)]
    public ParallelLoopResult Simple() =>
        Parallel.For(
            0,
            Count,
            _ =>
            {
                using var x = ObjectPool.GetObject();
            }
        );

}
