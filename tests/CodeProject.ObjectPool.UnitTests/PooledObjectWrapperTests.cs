// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.ObjectPool;
using NUnit.Framework;
using Shouldly;

namespace CodeProject.ObjectPool.UnitTests;

[TestFixture]
[Parallelizable]
internal sealed class PooledObjectWrapperTests
{
    [Test]
    public void Constructor_InternalResourceIsNotNull_ShouldBeAssignedToProperty()
    {
        // Arrange
        var internalResource = new List<int>();

        // Act
        var wrapper = new PooledObjectWrapper<List<int>>(internalResource);

        // Assert
        wrapper.InternalResource.ShouldBeSameAs(internalResource);
    }

    [Test]
    public void Create_InternalResourceIsNotNull_ShouldBeAssignedToProperty()
    {
        // Arrange
        var internalResource = new List<int>();

        // Act
        var wrapper = PooledObjectWrapper.Create(internalResource);

        // Assert
        wrapper.InternalResource.ShouldBeSameAs(internalResource);
    }

    [Test]
    public void OnResetState_ResettableResource_ShouldInvokeTryReset()
    {
        // Arrange
        var pool = new ObjectPool<PooledObjectWrapper<ResettableResource>>(() =>
            PooledObjectWrapper.Create(new ResettableResource(ResetResult.True)));

        // Act
        var wrapper = pool.GetObject();
        wrapper.Dispose();

        // Assert
        Assert.That(wrapper.InternalResource.ResetInvoked);
    }

    [Test]
    public void OnResetState_ResettableResource_ShouldReleaseResourceWhenResultIsFalse()
    {
        // Arrange
        var pool = new ObjectPool<PooledObjectWrapper<ResettableResource>>(() =>
            PooledObjectWrapper.Create(new ResettableResource(ResetResult.False)));

        // Act
        var wrapper = pool.GetObject();
        wrapper.Dispose();

        // Assert
        Assert.That(pool.ObjectsInPoolCount, Is.EqualTo(0));
    }

    [Test]
    public void OnResetState_ResettableResource_ShouldReleaseResourceWhenResetStateFailureExceptionIsThrown()
    {
        // Arrange
        var pool = new ObjectPool<PooledObjectWrapper<ResettableResource>>(() =>
            PooledObjectWrapper.Create(new ResettableResource(ResetResult.Throw)));

        // Act
        var wrapper = pool.GetObject();
        wrapper.Dispose();

        // Assert
        Assert.That(pool.ObjectsInPoolCount, Is.EqualTo(0));
    }

    private enum ResetResult
    {
        True,
        False,
        Throw
    }

    private sealed class ResettableResource(ResetResult resetResult) : IResettable
    {
        private readonly ResetResult _resetResult = resetResult;

        public bool ResetInvoked { get; private set; }

        public bool TryReset()
        {
            ResetInvoked = true;
            return _resetResult switch
            {
                ResetResult.True => true,
                ResetResult.False => false,
                ResetResult.Throw => throw new ResetStateFailureException(),
                _ => throw new InvalidDataException(),
            };
        }
    }
}
