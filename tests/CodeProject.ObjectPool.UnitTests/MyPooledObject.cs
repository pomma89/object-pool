// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace CodeProject.ObjectPool.UnitTests;

internal sealed class MyPooledObject : PooledObject, IEquatable<MyPooledObject>
{
    public override bool Equals(object? obj)
    {
        if (obj is null)
            return false;
        if (ReferenceEquals(this, obj))
            return true;
        return obj.GetType() == GetType() && Equals(obj as MyPooledObject);
    }

    public bool Equals(MyPooledObject? other)
    {
        return PooledObjectInfo.Id == other?.PooledObjectInfo.Id;
    }

    public override int GetHashCode() => PooledObjectInfo.GetHashCode();
}
