// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Concurrent;
using CodeProject.ObjectPool.Core;
using NUnit.Framework;
using Shouldly;

namespace CodeProject.ObjectPool.UnitTests;

[TestFixture]
internal sealed class ObjectPoolTests
{
    [Test]
    public void ShouldChangePoolLimitsIfCorrect()
    {
        var pool = new ObjectPool<MyPooledObject>();
        Assert.That(pool.MaximumPoolSize, Is.EqualTo(ObjectPool.DefaultPoolMaximumSize));

        pool.MaximumPoolSize *= 2;
        Assert.That(pool.MaximumPoolSize, Is.EqualTo(ObjectPool.DefaultPoolMaximumSize * 2));

        pool.MaximumPoolSize = 2;
        pool.MaximumPoolSize.ShouldBe(2);
    }

    [Test]
    public void ShouldEnforceLowerBoundOnPoolConstruction()
    {
        var pool = new ObjectPool<MyPooledObject>();
        pool.ObjectsInPoolCount.ShouldBe(0);
    }

    [TestCase(1, 1)]
    [TestCase(5, 1)]
    [TestCase(10, 1)]
    [TestCase(50, 1)]
    [TestCase(100, 1)]
    [TestCase(1, 2)]
    [TestCase(5, 2)]
    [TestCase(10, 2)]
    [TestCase(50, 2)]
    [TestCase(100, 2)]
    [TestCase(1, 4)]
    [TestCase(5, 4)]
    [TestCase(10, 4)]
    [TestCase(50, 4)]
    [TestCase(100, 4)]
    public void ShouldFillUntilMaximumSize(int maxSize, int multiplier)
    {
        var pool = new ObjectPool<MyPooledObject>(maxSize);
        var objectCount = maxSize * multiplier;
        var objects = new List<MyPooledObject>(objectCount);
        for (var i = 0; i < objectCount; ++i)
        {
            var obj = pool.GetObject();
            objects.Add(obj);
        }
        foreach (var obj in objects)
        {
            obj.Dispose();
        }
        Assert.That(pool.ObjectsInPoolCount, Is.EqualTo(maxSize));
    }

    [TestCase(1, 1)]
    [TestCase(5, 1)]
    [TestCase(10, 1)]
    [TestCase(50, 1)]
    [TestCase(100, 1)]
    [TestCase(1, 2)]
    [TestCase(5, 2)]
    [TestCase(10, 2)]
    [TestCase(50, 2)]
    [TestCase(100, 2)]
    [TestCase(1, 4)]
    [TestCase(5, 4)]
    [TestCase(10, 4)]
    [TestCase(50, 4)]
    [TestCase(100, 4)]
    public async Task ShouldFillUntilMaximumSize_Async(int maxSize, int multiplier)
    {
        var pool = new ObjectPool<MyPooledObject>(maxSize);
        var objectCount = maxSize * multiplier;
        var objects = new List<MyPooledObject>(objectCount);
        for (var i = 0; i < objectCount; ++i)
        {
            var obj = await pool.GetObjectAsync();
            objects.Add(obj);
        }
        foreach (var obj in objects)
        {
            obj.Dispose();
        }
        Assert.That(pool.ObjectsInPoolCount, Is.EqualTo(maxSize));
    }

    [TestCase(1, 1)]
    [TestCase(5, 1)]
    [TestCase(10, 1)]
    [TestCase(50, 1)]
    [TestCase(100, 1)]
    [TestCase(500, 1)]
    [TestCase(1, 2)]
    [TestCase(5, 2)]
    [TestCase(10, 2)]
    [TestCase(50, 2)]
    [TestCase(100, 2)]
    [TestCase(500, 2)]
    [TestCase(1, 4)]
    [TestCase(5, 4)]
    [TestCase(10, 4)]
    [TestCase(50, 4)]
    [TestCase(100, 4)]
    [TestCase(500, 4)]
    public void ShouldFillUntilMaximumSize_Parallel(int maxSize, int multiplier)
    {
        var pool = new ObjectPool<MyPooledObject>(maxSize);
        var objectCount = maxSize * multiplier;
        var objects = new MyPooledObject[objectCount];
        var parallelOptions = new ParallelOptions
        {
            MaxDegreeOfParallelism = Environment.ProcessorCount * multiplier
        };
        Parallel.For(
            0,
            objectCount,
            parallelOptions,
            i =>
            {
                objects[i] = pool.GetObject();
                objects[i].PooledObjectInfo.State.ShouldBe(PooledObjectState.Unavailable);
            }
        );
        Parallel.For(
            0,
            objectCount,
            i =>
            {
                objects[i].PooledObjectInfo.State.ShouldBe(PooledObjectState.Unavailable);
                objects[i].Dispose();
            }
        );
        Assert.That(pool.ObjectsInPoolCount, Is.EqualTo(maxSize));
    }

    [Test]
    public void ShouldHandleClearAfterNoUsage()
    {
        var pool = new ObjectPool<MyPooledObject>();

        pool.Clear();

        pool.ObjectsInPoolCount.ShouldBe(0);
    }

    [Test]
    public void ShouldHandleClearAfterSomeUsage()
    {
        var pool = new ObjectPool<MyPooledObject>();

        using (var obj = pool.GetObject()) { }

        pool.Clear();

        pool.ObjectsInPoolCount.ShouldBe(0);
    }

    [Test]
    public void ShouldHandleClearAndThenPoolCanBeUsedAgain()
    {
        var pool = new ObjectPool<MyPooledObject>();

        using (var obj = pool.GetObject()) { }

        pool.Clear();

        using (var obj = pool.GetObject()) { }

        pool.ObjectsInPoolCount.ShouldBe(1);
    }

    [Test]
    public void ShouldHandleClearAndThenReachCorrectSizeAtLaterUsage()
    {
        var pool = new ObjectPool<MyPooledObject>();

        using (var obj = pool.GetObject()) { }

        pool.Clear();

        // Usage #A
        using (var obj = pool.GetObject()) { }

        // Usages #B
        using (var obj = pool.GetObject()) { }
        using (var obj = pool.GetObject()) { }
        using (var obj = pool.GetObject()) { }

        // Despite usage #B, count should always be fixed.
        pool.ObjectsInPoolCount.ShouldBe(1);
    }

    [Test]
    public async Task ShouldHandleClearAndThenReachCorrectSizeAtLaterUsage_Async()
    {
        var pool = new ObjectPool<MyPooledObject>();

        using (var obj = await pool.GetObjectAsync()) { }

        pool.Clear();

        // Usage #A
        using (var obj = await pool.GetObjectAsync()) { }

        // Usages #B
        using (var obj = await pool.GetObjectAsync()) { }
        using (var obj = await pool.GetObjectAsync()) { }
        using (var obj = await pool.GetObjectAsync()) { }

        // Despite usage #B, count should always be fixed.
        pool.ObjectsInPoolCount.ShouldBe(1);
    }

    [TestCase(100, 2)]
    [TestCase(500, 2)]
    [TestCase(1000, 2)]
    [TestCase(5000, 2)]
    [TestCase(100, 4)]
    [TestCase(500, 4)]
    [TestCase(1000, 4)]
    [TestCase(5000, 4)]
    [TestCase(100, 8)]
    [TestCase(500, 8)]
    [TestCase(1000, 8)]
    [TestCase(5000, 8)]
    [TestCase(100, 16)]
    [TestCase(500, 16)]
    [TestCase(1000, 16)]
    [TestCase(5000, 16)]
    public void ShouldNotHaveRaceConditionsUnderHighLoad_Issue12(int maxSize, int multiplier)
    {
        var pool = new ObjectPool<MyPooledObject>(maxSize);
        var objectCount = maxSize * multiplier;
        var liveObjects = new ConcurrentDictionary<MyPooledObject, bool>();
        var parallelOptions = new ParallelOptions
        {
            MaxDegreeOfParallelism = Environment.ProcessorCount * multiplier
        };
        Parallel.For(
            0,
            objectCount,
            parallelOptions,
            i =>
            {
                var liveObject = pool.GetObject();
                liveObjects.TryAdd(liveObject, true).ShouldBeTrue();
                liveObject.PooledObjectInfo.State.ShouldBe(PooledObjectState.Unavailable);
                liveObjects.TryRemove(liveObject, out var _).ShouldBeTrue();
                liveObject.Dispose();
            }
        );
        liveObjects.Count.ShouldBe(0);
    }

    [Test]
    public void ShouldNotResetPooledObjectIdsWhenCleared()
    {
        var pool = new ObjectPool<MyPooledObject>();

        var poA = pool.GetObject();
        var poB = pool.GetObject();

        poA.PooledObjectInfo.Id.ShouldBe(1);
        poB.PooledObjectInfo.Id.ShouldBe(2);

        poA.Dispose();
        poB.Dispose();

        pool.ObjectsInPoolCount.ShouldBe(2);
        pool.Clear();
        pool.ObjectsInPoolCount.ShouldBe(0);

        var poC = pool.GetObject();

        poC.PooledObjectInfo.Id.ShouldBe(3);
    }

    [Test]
    public async Task ShouldNotResetPooledObjectIdsWhenCleared_Async()
    {
        var pool = new ObjectPool<MyPooledObject>();

        var poA = await pool.GetObjectAsync();
        var poB = await pool.GetObjectAsync();

        poA.PooledObjectInfo.Id.ShouldBe(1);
        poB.PooledObjectInfo.Id.ShouldBe(2);

        poA.Dispose();
        poB.Dispose();

        pool.ObjectsInPoolCount.ShouldBe(2);
        pool.Clear();
        pool.ObjectsInPoolCount.ShouldBe(0);

        var poC = await pool.GetObjectAsync();

        poC.PooledObjectInfo.Id.ShouldBe(3);
    }

    [TestCase(0)]
    [TestCase(-1)]
    [TestCase(-5)]
    [TestCase(-10)]
    public void ShouldThrowOnMaximumSizeEqualToZeroOrNegative(int maxSize)
    {
        Assert.Throws<ArgumentOutOfRangeException>(
            () => new ObjectPool<MyPooledObject>(maxSize)
        );
    }

    [TestCase(0)]
    [TestCase(-1)]
    [TestCase(-5)]
    [TestCase(-10)]
    public void ShouldThrowOnMaximumSizeEqualToZeroOrNegativeOnProperty(int maxSize)
    {
        Assert.Throws<ArgumentOutOfRangeException>(
            () => new ObjectPool<MyPooledObject> { MaximumPoolSize = maxSize }
        );
    }

    [Test]
    public void ShouldThrowUsingSpecifiedAsyncFactoryMethod()
    {
        var counter = 0;
        var pool = new ObjectPool<MyPooledObject>(_ =>
        {
            counter++;
            return Task.FromResult(new MyPooledObject());
        });

        Should.Throw(() => pool.GetObject(), typeof(InvalidOperationException));

        counter.ShouldBe(0);
    }

    [Test]
    public async Task ShouldUseSpecifiedAsyncFactoryMethod_Async()
    {
        var counter = 0;
        var pool = new ObjectPool<MyPooledObject>(_ =>
        {
            counter++;
            return Task.FromResult(new MyPooledObject());
        });

        using (var obj = await pool.GetObjectAsync()) { }

        counter.ShouldBe(1);
    }

    [Test]
    public void ShouldUseSpecifiedSyncFactoryMethod()
    {
        var counter = 0;
        var pool = new ObjectPool<MyPooledObject>(() =>
        {
            counter++;
            return new MyPooledObject();
        });

        using (var obj = pool.GetObject()) { }

        counter.ShouldBe(1);
    }

    [Test]
    public async Task ShouldUseSpecifiedSyncFactoryMethod_Async()
    {
        var counter = 0;
        var pool = new ObjectPool<MyPooledObject>(() =>
        {
            counter++;
            return new MyPooledObject();
        });

        using (var obj = await pool.GetObjectAsync()) { }

        counter.ShouldBe(1);
    }

    #region Diagnostics

    [Test]
    public async Task TotalInstancesCreated_ThrowingAsyncFactory_ShouldNotIncrementCounter()
    {
        // Arrange
        var pool = new ObjectPool<MyPooledObject>(
            asyncFactoryMethod: _ => throw new InvalidOperationException()
        );
        pool.Diagnostics.Enabled = true;

        // Act & Assert
        await Should.ThrowAsync<InvalidOperationException>(
            async () => await pool.GetObjectAsync()
        );
        pool.Diagnostics.Enabled.ShouldBeTrue();
        pool.Diagnostics.TotalInstancesCreated.ShouldBe(0L);
    }

    [Test]
    public void TotalInstancesCreated_ThrowingFactory_ShouldNotIncrementCounter()
    {
        // Arrange
        var pool = new ObjectPool<MyPooledObject>(
            factoryMethod: () => throw new InvalidOperationException()
        );
        pool.Diagnostics.Enabled = true;

        // Act & Assert
        Should.Throw<InvalidOperationException>(() => pool.GetObject());
        pool.Diagnostics.Enabled.ShouldBeTrue();
        pool.Diagnostics.TotalInstancesCreated.ShouldBe(0L);
    }

    #endregion Diagnostics

    #region Pooled object state

    [Test]
    public void PooledObjectStateShouldBecomeAvailableWhenReturnedToThePool()
    {
        var pool = new ObjectPool<MyPooledObject>();

        MyPooledObject obj;
        using (obj = pool.GetObject()) { }

        obj.PooledObjectInfo.State.ShouldBe(PooledObjectState.Available);
    }

    [Test]
    public async Task PooledObjectStateShouldBecomeAvailableWhenReturnedToThePool_Async()
    {
        var pool = new ObjectPool<MyPooledObject>();

        MyPooledObject obj;
        using (obj = await pool.GetObjectAsync()) { }

        obj.PooledObjectInfo.State.ShouldBe(PooledObjectState.Available);
    }

    [Test]
    public void PooledObjectStateShouldBecomeDisposedWhenCannotReturnToThePool()
    {
        var pool = new ObjectPool<MyPooledObject>(2);

        var obj = pool.GetObject();

        using (var tmp1 = pool.GetObject())
        using (var tmp2 = pool.GetObject()) { }

        obj.Dispose();
        obj.PooledObjectInfo.State.ShouldBe(PooledObjectState.Disposed);
    }

    [Test]
    public async Task PooledObjectStateShouldBecomeDisposedWhenCannotReturnToThePool_Async()
    {
        var pool = new ObjectPool<MyPooledObject>(2);

        var obj = await pool.GetObjectAsync();

        using (var tmp1 = await pool.GetObjectAsync())
        using (var tmp2 = await pool.GetObjectAsync()) { }

        obj.Dispose();
        obj.PooledObjectInfo.State.ShouldBe(PooledObjectState.Disposed);
    }

    [Test]
    public void PooledObjectStateShouldBecomeUnavailableWhenRetrievedFromThePool()
    {
        // Act
        var pool = new ObjectPool<MyPooledObject>();

        // Arrange
        using var obj = pool.GetObject();

        // Assert
        obj.PooledObjectInfo.State.ShouldBe(PooledObjectState.Unavailable);
    }

    [Test]
    public async Task PooledObjectStateShouldBecomeUnavailableWhenRetrievedFromThePool_Async()
    {
        // Act
        var pool = new ObjectPool<MyPooledObject>();

        // Arrange
        using var obj = await pool.GetObjectAsync();

        // Assert
        obj.PooledObjectInfo.State.ShouldBe(PooledObjectState.Unavailable);
    }

    #endregion Pooled object state
}
