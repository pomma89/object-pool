// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;

namespace CodeProject.ObjectPool.UnitTests;

[TestFixture, Parallelizable]
internal sealed class TimedObjectPoolTests
{
    [TestCase(0), TestCase(-1)]
    public void Constructor_WhenInvokedWithAsyncFactoryMethod_ShouldThrowWithNegativeOrZeroTimeout(int minutes)
    {
        // Arrange
        const int MaximumPoolSize = 100;
        var timeout = TimeSpan.FromMinutes(minutes);

        static Task<MyPooledObject> AsyncFactoryMethod(CancellationToken ct) => Task.FromResult(new MyPooledObject());

        // Act & Assert
        Assert.Throws<ArgumentOutOfRangeException>(() => new TimedObjectPool<MyPooledObject>(AsyncFactoryMethod, timeout));
        Assert.Throws<ArgumentOutOfRangeException>(() => new TimedObjectPool<MyPooledObject>(MaximumPoolSize, AsyncFactoryMethod, timeout));
    }

    [TestCase(0), TestCase(-1)]
    public void Constructor_WhenInvokedWithSyncFactoryMethod_ShouldThrowWithNegativeOrZeroTimeout(int minutes)
    {
        // Arrange
        const int MaximumPoolSize = 100;
        var timeout = TimeSpan.FromMinutes(minutes);

        static MyPooledObject FactoryMethod() => new();

        // Act & Assert
        Assert.Throws<ArgumentOutOfRangeException>(() => new TimedObjectPool<MyPooledObject>(FactoryMethod, timeout));
        Assert.Throws<ArgumentOutOfRangeException>(() => new TimedObjectPool<MyPooledObject>(MaximumPoolSize, FactoryMethod, timeout));
    }

    [Test]
    public async Task Constructor_WhenInvokedWithAsyncFactoryMethod_ShouldUseSpecifiedAsyncFactoryMethod()
    {
        // Arrange
        const int MaximumPoolSize = 100;
        var timeout = TimeSpan.FromMinutes(1);

        var counter = 0;
        Task<MyPooledObject> AsyncFactoryMethod(CancellationToken ct)
        {
            counter++;
            return Task.FromResult(new MyPooledObject());
        }

        // Act
        var timedObjectPool1 = new TimedObjectPool<MyPooledObject>(AsyncFactoryMethod, timeout);
        using var pooledObject1 = await timedObjectPool1.GetObjectAsync();
        var timedObjectPool2 = new TimedObjectPool<MyPooledObject>(MaximumPoolSize, AsyncFactoryMethod, timeout);
        using var pooledObject2 = await timedObjectPool2.GetObjectAsync();

        // Assert
        Assert.That(counter, Is.EqualTo(2));
    }

    [Test]
    public void Constructor_WhenInvokedWithSyncFactoryMethod_ShouldUseSpecifiedFactoryMethod()
    {
        // Arrange
        const int MaximumPoolSize = 100;
        var timeout = TimeSpan.FromMinutes(1);

        var counter = 0;
        MyPooledObject FactoryMethod()
        {
            counter++;
            return new MyPooledObject();
        }

        // Act
        var timedObjectPool1 = new TimedObjectPool<MyPooledObject>(FactoryMethod, timeout);
        using var pooledObject1 = timedObjectPool1.GetObject();
        var timedObjectPool2 = new TimedObjectPool<MyPooledObject>(MaximumPoolSize, FactoryMethod, timeout);
        using var pooledObject2 = timedObjectPool2.GetObject();

        // Assert
        Assert.That(counter, Is.EqualTo(2));
    }
}
