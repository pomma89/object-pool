// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using CodeProject.ObjectPool.Core;
using CodeProject.ObjectPool.Specialized;
using Newtonsoft.Json;
using NUnit.Framework;
using Shouldly;

namespace CodeProject.ObjectPool.UnitTests.Specialized;

[TestFixture]
internal sealed class MemoryStreamPoolTests
{
    private readonly IMemoryStreamPool _memoryStreamPool = MemoryStreamPool.Instance;

    [Test]
    public void IdPropertyShouldNotChangeUsageAfterUsage()
    {
        // First usage.
        int id;
        using (var pms = _memoryStreamPool.GetObject())
        {
            id = pms.PooledObjectInfo.Id;
            id.ShouldNotBe(0);
        }

        // Second usage is the same, pool uses a sort of stack, not a proper queue.
        using (var pms = _memoryStreamPool.GetObject())
        {
            pms.PooledObjectInfo.Id.ShouldBe(id);
        }
    }

    [SetUp]
    public void SetUp()
    {
        _memoryStreamPool.Clear();
        _memoryStreamPool.Diagnostics = new ObjectPoolDiagnostics { Enabled = true };
    }

    [TestCase(10)]
    [TestCase(100)]
    [TestCase(1000)]
    public void ShouldAllowCommonUsingPattern_ManyTimesWithJsonReaderAndWriter(int count)
    {
        var jsonSerializer = new JsonSerializer();

        for (var i = 1; i <= count; ++i)
        {
            var text = Utils.Faker.Lorem.Paragraphs((i % 10) + 1);

            using var ms = _memoryStreamPool.GetObject().MemoryStream;
            using var sw = new StreamWriter(ms);
            using var jw = new JsonTextWriter(sw);
            jsonSerializer.Serialize(jw, text);
            jw.Flush();

            ms.Position = 0L;

            using var sr = new StreamReader(ms);
            using var jr = new JsonTextReader(sr);
            jsonSerializer.Deserialize<string>(jr).ShouldBe(text);
        }
    }

    [TestCase(10)]
    [TestCase(100)]
    [TestCase(1000)]
    public void ShouldAllowCommonUsingPattern_ManyTimesWithStringReaderAndWriter(int count)
    {
        for (var i = 1; i <= count; ++i)
        {
            var text = Utils.Faker.Lorem.Paragraphs((i % 10) + 1);

            using var ms = _memoryStreamPool.GetObject().MemoryStream;
            using var sw = new StreamWriter(ms);
            sw.Write(text);
            sw.Flush();

            ms.Position = 0L;

            using var sr = new StreamReader(ms);
            sr.ReadToEnd().ShouldBe(text);
        }
    }

    [Test]
    public void ShouldClearPoolWhenMaxCapacityIsDecreased()
    {
        int initialId;
        using (var pms = _memoryStreamPool.GetObject())
        {
            initialId = pms.PooledObjectInfo.Id;
        }

        initialId.ShouldBeGreaterThan(0);

        _memoryStreamPool.MaximumMemoryStreamCapacity /= 2;
        using (var pms = _memoryStreamPool.GetObject())
        {
            pms.PooledObjectInfo.Id.ShouldBeGreaterThan(initialId);
        }
    }

    [Test]
    public void ShouldClearPoolWhenMinCapacityIsIncreased()
    {
        int initialCapacity;
        using (var pms = _memoryStreamPool.GetObject())
        {
            initialCapacity = pms.MemoryStream.Capacity;
        }

        initialCapacity.ShouldBe(_memoryStreamPool.MinimumMemoryStreamCapacity);

        _memoryStreamPool.MinimumMemoryStreamCapacity = initialCapacity * 2;
        using (var pms = _memoryStreamPool.GetObject())
        {
            pms.MemoryStream.Capacity.ShouldBe(_memoryStreamPool.MinimumMemoryStreamCapacity);
        }
    }

    [Test]
    public void ShouldNotClearPoolWhenMaxCapacityIsIncreased()
    {
        int initialId;
        using (var pms = _memoryStreamPool.GetObject())
        {
            initialId = pms.PooledObjectInfo.Id;
        }

        initialId.ShouldBeGreaterThan(0);

        _memoryStreamPool.MaximumMemoryStreamCapacity *= 2;
        using (var pms = _memoryStreamPool.GetObject())
        {
            pms.PooledObjectInfo.Id.ShouldBe(initialId);
        }
    }

    [Test]
    public void ShouldNotClearPoolWhenMinCapacityIsDecreased()
    {
        int initialCapacity;
        using (var pms = _memoryStreamPool.GetObject())
        {
            initialCapacity = pms.MemoryStream.Capacity;
        }

        initialCapacity.ShouldBe(_memoryStreamPool.MinimumMemoryStreamCapacity);

        _memoryStreamPool.MinimumMemoryStreamCapacity = initialCapacity / 2;
        using (var pms = _memoryStreamPool.GetObject())
        {
            pms.MemoryStream.Capacity.ShouldBe(initialCapacity);
        }
    }

    [Test]
    public void ShouldNotReturnToPoolWhenStreamIsLarge()
    {
        var text = Utils.Faker.Lorem.Paragraphs(5000);

        string result;
        using (var pms = _memoryStreamPool.GetObject())
        {
            var sw = new StreamWriter(pms.MemoryStream);
            sw.Write(text);
            sw.Write(text);
            sw.Flush();

            pms.MemoryStream.Position = 0L;

            var sr = new StreamReader(pms.MemoryStream);
            result = sr.ReadToEnd();

            pms.MemoryStream.Capacity.ShouldBeGreaterThan(
                _memoryStreamPool.MaximumMemoryStreamCapacity
            );
        }

        result.ShouldBe(text + text);

        _memoryStreamPool.ObjectsInPoolCount.ShouldBe(0);
        _memoryStreamPool.Diagnostics.ReturnedToPoolCount.ShouldBe(0);
        _memoryStreamPool.Diagnostics.ObjectResetFailedCount.ShouldBe(1);
    }

    [Test]
    public void ShouldNotReturnToPoolWhenStreamIsLargeAndStreamIsManuallyDisposed()
    {
        var text = Utils.Faker.Lorem.Paragraphs(5000);

        string result;
        var pms = _memoryStreamPool.GetObject();

        var sw = new StreamWriter(pms.MemoryStream);
        sw.Write(text);
        sw.Write(text);
        sw.Flush();

        pms.MemoryStream.Position = 0L;

        var sr = new StreamReader(pms.MemoryStream);
        result = sr.ReadToEnd();

        pms.MemoryStream.Capacity.ShouldBeGreaterThan(
            _memoryStreamPool.MaximumMemoryStreamCapacity
        );
        pms.MemoryStream.Dispose();

        result.ShouldBe(text + text);

        _memoryStreamPool.ObjectsInPoolCount.ShouldBe(0);
        _memoryStreamPool.Diagnostics.ReturnedToPoolCount.ShouldBe(0);
        _memoryStreamPool.Diagnostics.ObjectResetFailedCount.ShouldBe(1);
    }

    [TestCase("a&b")]
    [TestCase("SNAU ORSO birretta")]
    [TestCase("PU <3 PI")]
    [TestCase(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget ante risus. In rhoncus mattis leo, in tincidunt felis euismod sed. Pellentesque rhoncus elementum lacus tincidunt feugiat. Interdum et malesuada fames ac ante ipsum primis in faucibus. --> Aliquam scelerisque, SNAU."
    )]
    [TestCase(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget ante risus. In rhoncus mattis leo, in tincidunt felis euismod sed. Pellentesque rhoncus elementum lacus tincidunt feugiat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam scelerisque, lorem ac pretium luctus, nunc dui tincidunt sem, id rutrum nibh urna a neque. Maecenas lacus tellus, scelerisque nec faucibus ac, dignissim non justo. Vivamus volutpat at metus hendrerit feugiat. Donec imperdiet lobortis est a efficitur. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas."
    )]
    public void ShouldReturnToPoolWhenStreamIsSmall(string text)
    {
        string result;
        using (var pms = _memoryStreamPool.GetObject())
        {
            var sw = new StreamWriter(pms.MemoryStream);
            sw.Write(text);
            sw.Flush();

            pms.MemoryStream.Position = 0L;

            var sr = new StreamReader(pms.MemoryStream);
            result = sr.ReadToEnd();

            pms.MemoryStream.Capacity.ShouldBeLessThanOrEqualTo(
                _memoryStreamPool.MaximumMemoryStreamCapacity
            );
        }

        result.ShouldBe(text);

        _memoryStreamPool.ObjectsInPoolCount.ShouldBe(1);
        _memoryStreamPool.Diagnostics.ReturnedToPoolCount.ShouldBe(1);
        _memoryStreamPool.Diagnostics.ObjectResetFailedCount.ShouldBe(0);
    }

    [TestCase("a&b")]
    [TestCase("SNAU ORSO birretta")]
    [TestCase("PU <3 PI")]
    [TestCase(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget ante risus. In rhoncus mattis leo, in tincidunt felis euismod sed. Pellentesque rhoncus elementum lacus tincidunt feugiat. Interdum et malesuada fames ac ante ipsum primis in faucibus. --> Aliquam scelerisque, SNAU."
    )]
    [TestCase(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget ante risus. In rhoncus mattis leo, in tincidunt felis euismod sed. Pellentesque rhoncus elementum lacus tincidunt feugiat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam scelerisque, lorem ac pretium luctus, nunc dui tincidunt sem, id rutrum nibh urna a neque. Maecenas lacus tellus, scelerisque nec faucibus ac, dignissim non justo. Vivamus volutpat at metus hendrerit feugiat. Donec imperdiet lobortis est a efficitur. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas."
    )]
    public void ShouldReturnToPoolWhenStreamIsSmall_TwoTimes(string text)
    {
        string result;
        using (var pms = _memoryStreamPool.GetObject())
        {
            var sw = new StreamWriter(pms.MemoryStream);
            sw.Write(text);
            sw.Flush();

            pms.MemoryStream.Position = 0L;

            var sr = new StreamReader(pms.MemoryStream);
            result = sr.ReadToEnd();

            pms.MemoryStream.Capacity.ShouldBeLessThanOrEqualTo(
                _memoryStreamPool.MaximumMemoryStreamCapacity
            );
        }
        result.ShouldBe(text);

        using (var pms = _memoryStreamPool.GetObject())
        {
            var sw = new StreamWriter(pms.MemoryStream);
            sw.Write(text);
            sw.Write(text);
            sw.Flush();

            pms.MemoryStream.Position = 0L;

            var sr = new StreamReader(pms.MemoryStream);
            result = sr.ReadToEnd();

            pms.MemoryStream.Capacity.ShouldBeLessThanOrEqualTo(
                _memoryStreamPool.MaximumMemoryStreamCapacity
            );
        }
        result.ShouldBe(text + text);

        _memoryStreamPool.ObjectsInPoolCount.ShouldBe(1);
        _memoryStreamPool.Diagnostics.ReturnedToPoolCount.ShouldBe(2);
        _memoryStreamPool.Diagnostics.ObjectResetFailedCount.ShouldBe(0);
    }

    [TestCase("a&b")]
    [TestCase("SNAU ORSO birretta")]
    [TestCase("PU <3 PI")]
    [TestCase(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget ante risus. In rhoncus mattis leo, in tincidunt felis euismod sed. Pellentesque rhoncus elementum lacus tincidunt feugiat. Interdum et malesuada fames ac ante ipsum primis in faucibus. --> Aliquam scelerisque, SNAU."
    )]
    [TestCase(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget ante risus. In rhoncus mattis leo, in tincidunt felis euismod sed. Pellentesque rhoncus elementum lacus tincidunt feugiat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam scelerisque, lorem ac pretium luctus, nunc dui tincidunt sem, id rutrum nibh urna a neque. Maecenas lacus tellus, scelerisque nec faucibus ac, dignissim non justo. Vivamus volutpat at metus hendrerit feugiat. Donec imperdiet lobortis est a efficitur. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas."
    )]
    public void ShouldReturnToPoolWhenStreamIsSmallAndStreamIsManuallyDisposed(string text)
    {
        string result;
        var pms = _memoryStreamPool.GetObject();

        var sw = new StreamWriter(pms.MemoryStream);
        sw.Write(text);
        sw.Flush();

        pms.MemoryStream.Position = 0L;

        var sr = new StreamReader(pms.MemoryStream);
        result = sr.ReadToEnd();

        pms.MemoryStream.Capacity.ShouldBeLessThanOrEqualTo(
            _memoryStreamPool.MaximumMemoryStreamCapacity
        );
        pms.MemoryStream.Dispose();

        result.ShouldBe(text);

        _memoryStreamPool.ObjectsInPoolCount.ShouldBe(1);
        _memoryStreamPool.Diagnostics.ReturnedToPoolCount.ShouldBe(1);
        _memoryStreamPool.Diagnostics.ObjectResetFailedCount.ShouldBe(0);
    }

    [TestCase("a&b")]
    [TestCase("SNAU ORSO birretta")]
    [TestCase("PU <3 PI")]
    [TestCase(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget ante risus. In rhoncus mattis leo, in tincidunt felis euismod sed. Pellentesque rhoncus elementum lacus tincidunt feugiat. Interdum et malesuada fames ac ante ipsum primis in faucibus. --> Aliquam scelerisque, SNAU."
    )]
    [TestCase(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget ante risus. In rhoncus mattis leo, in tincidunt felis euismod sed. Pellentesque rhoncus elementum lacus tincidunt feugiat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam scelerisque, lorem ac pretium luctus, nunc dui tincidunt sem, id rutrum nibh urna a neque. Maecenas lacus tellus, scelerisque nec faucibus ac, dignissim non justo. Vivamus volutpat at metus hendrerit feugiat. Donec imperdiet lobortis est a efficitur. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas."
    )]
    public void ShouldReturnToPoolWhenStreamIsSmallAndStreamIsManuallyDisposed_TwoTimes(
        string text
    )
    {
        string result;
        // First
        var pms = _memoryStreamPool.GetObject();
        var sw = new StreamWriter(pms.MemoryStream);
        sw.Write(text);
        sw.Flush();

        pms.MemoryStream.Position = 0L;

        var sr = new StreamReader(pms.MemoryStream);
        result = sr.ReadToEnd();

        pms.MemoryStream.Capacity.ShouldBeLessThanOrEqualTo(
            _memoryStreamPool.MaximumMemoryStreamCapacity
        );
        pms.MemoryStream.Dispose();
        result.ShouldBe(text);

        // Second
        pms = _memoryStreamPool.GetObject();
        sw = new StreamWriter(pms.MemoryStream);
        sw.Write(text);
        sw.Write(text);
        sw.Flush();

        pms.MemoryStream.Position = 0L;

        sr = new StreamReader(pms.MemoryStream);
        result = sr.ReadToEnd();

        pms.MemoryStream.Capacity.ShouldBeLessThanOrEqualTo(
            _memoryStreamPool.MaximumMemoryStreamCapacity
        );
        pms.MemoryStream.Dispose();
        result.ShouldBe(text + text);

        _memoryStreamPool.ObjectsInPoolCount.ShouldBe(1);
        _memoryStreamPool.Diagnostics.ReturnedToPoolCount.ShouldBe(2);
        _memoryStreamPool.Diagnostics.ObjectResetFailedCount.ShouldBe(0);
    }

    [Test]
    public void ToString_NonZeroLength_ShouldPrintLengthInResultString()
    {
        // Arrange
        using var pms = _memoryStreamPool.GetObject();
        using var sw = new StreamWriter(pms.MemoryStream);
        sw.Write("Lorem ipsum");
        sw.Flush();

        // Act
        var toString = pms.ToString();

        // Assert
        pms.MemoryStream.Length.ShouldBeGreaterThan(0);
        toString.ShouldContain(pms.MemoryStream.Length.ToString());
    }

    [Test]
    public void ToString_NonZeroPosition_ShouldPrintPositionInResultString()
    {
        // Arrange
        using var pms = _memoryStreamPool.GetObject();
        using var sw = new StreamWriter(pms.MemoryStream);
        sw.Write("Lorem ipsum");
        sw.Flush();

        // Act
        var toString = pms.ToString();

        // Assert
        pms.MemoryStream.Position.ShouldBeGreaterThan(0);
        toString.ShouldContain(pms.MemoryStream.Position.ToString());
    }
}
