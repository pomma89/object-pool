// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using CodeProject.ObjectPool.Core;
using NUnit.Framework;
using Shouldly;

namespace CodeProject.ObjectPool.UnitTests.Core;

[TestFixture, Parallelizable]
internal sealed class PooledObjectValidationContextTests
{
    [Test]
    public void Inbound_MyPooledObject_ShouldContainValidInformation()
    {
        // Arrange
        var myPooledObject = new MyPooledObject();

        // Act
        var validationContext = PooledObjectValidationContext.Inbound(myPooledObject);

        // Assert
        validationContext.Direction.ShouldBe(PooledObjectDirection.Inbound);
        validationContext.PooledObject.ShouldBeSameAs(myPooledObject);
        validationContext.PooledObjectInfo.ShouldBeSameAs(myPooledObject.PooledObjectInfo);
    }

    [Test]
    public void Outbound_MyPooledObject_ShouldContainValidInformation()
    {
        // Arrange
        var myPooledObject = new MyPooledObject();

        // Act
        var validationContext = PooledObjectValidationContext.Outbound(myPooledObject);

        // Assert
        validationContext.Direction.ShouldBe(PooledObjectDirection.Outbound);
        validationContext.PooledObject.ShouldBeSameAs(myPooledObject);
        validationContext.PooledObjectInfo.ShouldBeSameAs(myPooledObject.PooledObjectInfo);
    }
}
